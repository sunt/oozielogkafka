package com.sqlTest

import java.util.{Date, Properties}
import com.google.gson.Gson
import com.sqlTest.model.LogModel
import com.sqlTest.util._
import com.twitter.app.App
import com.twitter.util.Future
import kafka.producer.{Producer}
import kafka.producer.KeyedMessage
import org.apache.commons.httpclient.util.DateUtil

/**
 * Created by sunteng on 2016/2/29.
 */


object OozieKafka extends App {

  /**
   *
   * @param job_task_id job_task_id
   * @param file_name 文件名
   * @param log_order 日志执行步骤
   * @param log_info 日志信息
   * @return
   */
  def writeToKafka(job_task_id: String, file_name: String, log_order: String, log_info: String): Future[Boolean] = {
    val jon_task_id = new UrlEncodeUtil().EncodeParam(job_task_id)

    val fileName = new UrlEncodeUtil().EncodeParam(file_name)
    val logOrder = new UrlEncodeUtil().EncodeParam(log_order)

    val logInfo = new UrlEncodeUtil().EncodeParam(log_info)
    val log = if (logInfo.equals("")) "Empty" else logInfo

    //    val resultFuture: Future[Boolean] = toKafka(job_task_id, message).flatMap(queryResult => convertDBResultToBoolean(queryResult))
    val resultFuture: Future[Boolean] = toKafka(jon_task_id, fileName, logOrder, log)
    resultFuture
  }

  /**
   *
   * @param job_task_id job_task_id
   * @param file_name 文件名
   * @param log_order 日志执行步骤
   * @param log_info 日志信息
   */
  def toKafka(job_task_id: String, file_name: String, log_order: String, log_info: String): Future[Boolean] = {
    var result: Boolean = false
    try {
      val hbaseUtil = HbaseUtil
      val p: Properties = new LoadProperUtil().getProperties
      val hbaseP: Properties = new LoadProperUtil().getHbaseProperties
      val tableName = "task_log"
      val columFamily = "files_log"


      val topic = p.getProperty("topic")
      val logModel = new LogModel()
      logModel.setFile_name(file_name)
      logModel.setJob_task_id(job_task_id)
      logModel.setLog_info(log_info)
      logModel.setLog_order(log_order)
      //      val messageMap = Map("job_task_id" -> job_task_id, "start_time" -> start_time, "file_name" -> file_name, "log_order" -> log_order, "log_info" -> log_info)
      val gson = new Gson()
      val messageJson = gson.toJson(logModel)

      val producer: Producer[String, String] = LoadKafkaUtil.loadKafka()
      val data = new KeyedMessage[String, String](topic, messageJson)
      producer.send(data)
      producer.close()

      hbaseUtil.createTable(tableName, columFamily)
      hbaseUtil.insert(tableName, job_task_id, columFamily, "input", file_name);
      hbaseUtil.insert(tableName, job_task_id, columFamily, log_order, log_info);

      result = true
    }
    catch {
      case e: Exception =>
        e.printStackTrace()
        result = false
    }
    Future.value(result)
  }
}