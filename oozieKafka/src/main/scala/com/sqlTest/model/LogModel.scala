package com.sqlTest.model

/**
 * Created by sunteng on 2016/6/6.
 */
class LogModel {
  private var job_task_id: String = null
  private var start_time: String = null
  private var file_name: String = null
  private var log_order: String = null
  private var log_info: String = null

  def getJob_task_id: String = {
    return job_task_id
  }

  def setJob_task_id(job_task_id: String) {
    this.job_task_id = job_task_id
  }

  def getStart_time: String = {
    return start_time
  }

  def setStart_time(start_time: String) {
    this.start_time = start_time
  }

  def getFile_name: String = {
    return file_name
  }

  def setFile_name(file_name: String) {
    this.file_name = file_name
  }

  def getLog_order: String = {
    return log_order
  }

  def setLog_order(log_order: String) {
    this.log_order = log_order
  }

  def getLog_info: String = {
    return log_info
  }

  def setLog_info(log_info: String) {
    this.log_info = log_info
  }

  override def toString: String = {
    return "LogModel{" + "job_task_id='" + job_task_id + '\'' + ", start_time='" + start_time + '\'' + ", file_name='" + file_name + '\'' + ", log_order='" + log_order + '\'' + ", log_info='" + log_info + '\'' + '}'
  }

}
