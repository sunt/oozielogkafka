package com.sqlTest.util

import java.util.Properties

import kafka.producer.{Producer, ProducerConfig}

/**
 * Created by sunteng on 2016/5/18.
 */
object LoadKafkaUtil {
  /**
   * 加载kafka
   * @return
   */
  def loadKafka(): Producer[String, String] = {
    val p: Properties = new LoadProperUtil().getProperties
    val props: Properties = new Properties
    props.put("metadata.broker.list", p.getProperty("metadata.broker.list"))
    props.put("serializer.class", p.getProperty("serializer.class"))
    props.put("producer.type", p.getProperty("producer.type"))
    props.put("request.required.acks", p.getProperty("request.required.acks"))
    props.put("partitioner.class", p.getProperty("partitioner.class"))
    props.put("numPartitions", p.getProperty("numPartitions"))

    val config: ProducerConfig = new ProducerConfig(props)
    val producer: Producer[String, String] = new Producer[String, String](config)
    producer
  }
}
