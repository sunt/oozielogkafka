package com.sqlTest.util

import java.text.SimpleDateFormat
import java.util.Date

/**
 * Created by sunteng on 2016/6/6.
 */
class DateParseUtil {
  def dateParse(arg: String): String = {
    val format: SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")

    format.format(arg.toLong)
  }

}
