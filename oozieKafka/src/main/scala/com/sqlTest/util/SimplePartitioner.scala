package com.sqlTest.util

import kafka.producer.Partitioner
import kafka.utils.{Utils, VerifiableProperties}

/**
 * Created by sunteng on 2016/5/19.
 */
class SimplePartitioner(props: VerifiableProperties = null) extends Partitioner {
  private val random = new java.util.Random

  /**
   *
   * @param key
   * @param numPartitions
   * @return
   */
  def partition(key: Any, numPartitions: Int): Int = {
    var partition: Int = 0
    val stringKey: String = key.toString
    val offset: Int = stringKey.lastIndexOf('.')
    if (offset > 0) {
      partition = Integer.parseInt(stringKey.substring(offset + 1)) % numPartitions
    }
    partition
  }
}
