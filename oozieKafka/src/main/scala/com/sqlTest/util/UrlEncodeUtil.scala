package com.sqlTest.util

import java.net.URLDecoder

import kafka.utils.Json


/**
 * Created by sunteng on 2016/5/26.
 */
class UrlEncodeUtil {
  def EncodeParam(param: String): String = {
    val utf8_res: String = URLDecoder.decode(param, "UTF-8")
    utf8_res
  }
}
