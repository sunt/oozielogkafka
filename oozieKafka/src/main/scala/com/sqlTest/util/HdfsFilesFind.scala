package com.sqlTest.util

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.FileStatus
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.fs.Path
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.List

import scala.collection.mutable.ListBuffer

/**
 * Created by sunteng on 2016/6/8.
 */
class HdfsFilesFind {
  private val logger: Logger = LoggerFactory.getLogger(classOf[HdfsFilesFind])
  private val conf: Configuration = new Configuration

  /**
   * 查找指定目下hdfs文件
   *
   * @param baseDirName
   * @param fileList
   * @throws Exception
   */
  @throws(classOf[Exception])
  def findFiles(baseDirName: String, fileList: ListBuffer[FileStatus]) {
    var flag: Boolean = false

    val srcPath: Path = new Path(baseDirName)
    logger.info(srcPath.toString)
    val srcFs: FileSystem = srcPath.getFileSystem(conf)
    flag = srcFs.exists(srcPath)
    if (!flag) {
      logger.info(baseDirName)
    }
    else {
      val status: Array[FileStatus] = srcFs.listStatus(srcPath)
      for (i <- status) {
        if (!i.isDirectory) {
          logger.info(i.toString())
          fileList += i
        }
        else if (i.isDirectory) {
          findFiles(i.getPath.toString, fileList)
        }
      }

    }
  }
}