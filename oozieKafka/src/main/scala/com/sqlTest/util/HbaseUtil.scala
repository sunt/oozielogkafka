package com.sqlTest.util

import org.apache.hadoop.hbase.{HColumnDescriptor, HTableDescriptor, HBaseConfiguration}
import org.apache.hadoop.hbase.client.{HBaseAdmin, Put, HTable}
import org.apache.hadoop.hbase.util.Bytes

/**
 * Created by sunteng on 2016/6/8.
 */
object HbaseUtil {
  val conf = HBaseConfiguration.create()
  conf.set("hbase.zookeeper.quorum", "192.168.2.254,192.168.2.252,192.168.2.250")
  conf.set("hbase.zookeeper.property.clientPort", "2181")

  // 创建数据库表
  def createTable(tableName: String, columnFamily: String) {
    // 新建一个数据库管理员
    val hAdmin: HBaseAdmin = new HBaseAdmin(conf)

    if (hAdmin.tableExists(tableName)) {
      System.out.println("表已经存在")
    } else {
      // 新建一个 scores 表的描述
      val tableDesc: HTableDescriptor = new HTableDescriptor(tableName);
      // 在描述里添加列族
      tableDesc.addFamily(new HColumnDescriptor(columnFamily));

      // 根据配置好的描述建表
      hAdmin.createTable(tableDesc);
      System.out.println("创建表成功");
    }
  }

  /**
   * 插入数据
   * @param tableName
   * @param rowKey
   * @param cf
   * @param key
   * @param value
   */
  def insert(tableName: String, rowKey: String, cf: String, key: String, value: String): Unit = {
    val table = new HTable(conf, tableName)

    val theput = new Put(Bytes.toBytes(rowKey))
    theput.add(Bytes.toBytes(cf), Bytes.toBytes(key), Bytes.toBytes(value))
    table.put(theput)
  }

  /**
   * 删除数据库表
   * @param tableName
   */
  def deleteTable(tableName: String) {
    // 新建一个数据库管理员
    val hAdmin: HBaseAdmin = new HBaseAdmin(conf)

    if (hAdmin.tableExists(tableName)) {
      // 关闭一个表
      hAdmin.disableTable(tableName)
      // 删除一个表
      hAdmin.deleteTable(tableName)
      System.out.println("删除表成功")

    } else {
      System.out.println("删除的表不存在")
      System.exit(0)
    }
  }
}
