package com.sqlTest.util

import java.io.{IOException, FileInputStream}
import java.util.Properties

/**
 * Created by sunteng on 2016/5/17.
 */
class LoadProperUtil {
  /**
   *
   * @return
   */
  def getProperties: Properties = {
    val p: Properties = new Properties
    try {
      p.load(classOf[LoadProperUtil].getClassLoader.getResourceAsStream("kafka.properties"))
    }
    catch {
      case e: IOException => {
        e.printStackTrace
      }
    }
    p
  }
  def getHbaseProperties: Properties = {
    val p: Properties = new Properties
    try {
      p.load(classOf[LoadProperUtil].getClassLoader.getResourceAsStream("hbase.properties"))
    }
    catch {
      case e: IOException => {
        e.printStackTrace
      }
    }
    p
  }
}
