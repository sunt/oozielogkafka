package com.sqlTest

import java.security.MessageDigest

/**
 * Created by sunteng on 2016/3/1.
 */
object MD5 {
  def hash(s: String): String = {
    val md = MessageDigest.getInstance("MD5")
    md.update(s.getBytes())
    val b = md.digest()
    var i: Int = 0
    val sb: StringBuffer = new StringBuffer()
    for (a <- 0 until b.length) {
      i = b(a)
      if (i < 0)
        i += 256
      if (i < 16)
        sb.append("0")
      sb.append(Integer.toHexString(i))
    }
    return sb.toString
  }
}
