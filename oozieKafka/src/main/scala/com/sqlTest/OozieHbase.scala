package com.sqlTest

import java.util
import java.util.{List, Properties}

import com.sqlTest.util._
import com.twitter.app.App
import com.twitter.logging.Logger
import com.twitter.util.Future
import kafka.producer.{KeyedMessage, Producer}
import org.apache.hadoop.fs.FileStatus
import org.apache.hadoop.hbase.HBaseConfiguration
import org.apache.hadoop.hbase.client.{Put, HTable, HBaseAdmin}
import org.apache.hadoop.hbase.util.Bytes

import scala.collection.mutable.ListBuffer

/**
 * Created by sunteng on 2016/2/29.
 */


object OozieHbase extends App {
  val log = Logger.apply()

  /**
   *
   * @param user
   * @param job_id
   * @param start_time
   * @param end_time
   * @param state
   * @return
   */
  def writeHbase(user: String, job_id: String, start_time: String, end_time: String, state: String): Future[Boolean] = {
    val startTime = new DateParseUtil().dateParse(start_time)
    val endTime = new DateParseUtil().dateParse(end_time)

    //    val resultFuture: Future[Boolean] = toKafka(job_task_id, message).flatMap(queryResult => convertDBResultToBoolean(queryResult))
    val resultFuture: Future[Boolean] = toHbase(user, job_id, startTime, endTime, state)
    resultFuture
  }

  /**
   *
   * @param user
   * @param job_id
   * @param start_time
   * @param end_time
   * @param state
   * @return
   */
  def toHbase(user: String, job_id: String, start_time: String, end_time: String, state: String): Future[Boolean] = {
    var result: Boolean = false
    val split = new ConstUtil().split
    try {
      val hbaseUtil = HbaseUtil
      val p: Properties = new LoadProperUtil().getHbaseProperties
      val tableName = "workflow_log_summary"
      val columnFamily = "cf"
      hbaseUtil.createTable(tableName, columnFamily)
      hbaseUtil.insert(tableName, user + split + job_id + split + start_time + split + end_time + split + state, columnFamily, "", "");
      //      new HbaseUtil().insert("log-aggregation", job_task_id, "files_log", "output", fileList.toList.toString())
      result = true
    }
    catch {
      case e: Exception =>
        e.printStackTrace()
        result = false
    }
    Future.value(result)
  }
}