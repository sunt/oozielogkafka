package com.sqlTest

import java.util.Properties

import com.sqlTest.util._
import com.twitter.app.App
import com.twitter.util.Future

/**
 * Created by sunteng on 2016/2/29.
 */


object OozieResultHbase extends App {

  /**
   *
   * @param job_task_id
   * @param start_time
   * @param end_time
   * @param status
   * @param output
   * @return
   */
  def writeHbase(job_task_id: String, start_time: String, end_time: String, status: String, output: String): Future[Boolean] = {

    val startTime = new DateParseUtil().dateParse(start_time)
    val endTime = new DateParseUtil().dateParse(end_time)
    //    val resultFuture: Future[Boolean] = toKafka(job_task_id, message).flatMap(queryResult => convertDBResultToBoolean(queryResult))
    val resultFuture: Future[Boolean] = toHbase(job_task_id, startTime, endTime, status, output)
    resultFuture
  }

  /**
   *
   * @param job_task_id
   * @param startTime
   * @param endTime
   * @param status
   * @param output
   * @return
   */
  def toHbase(job_task_id: String, startTime: String, endTime: String, status: String, output: String): Future[Boolean] = {
    var result: Boolean = false
    try {
      val p: Properties = new LoadProperUtil().getHbaseProperties
      val tableName = "task_log"
      val columFamily = "files_log"

      val hbaseUtil = HbaseUtil
      hbaseUtil.createTable(tableName, columFamily)
      hbaseUtil.insert(tableName, job_task_id, columFamily, "start_time", startTime)
      hbaseUtil.insert(tableName, job_task_id, columFamily, "end_time", endTime)
      hbaseUtil.insert(tableName, job_task_id, columFamily, "status", status)
      hbaseUtil.insert(tableName, job_task_id, columFamily, "output", output)
      result = true
    }
    catch {
      case e: Exception =>
        e.printStackTrace()
        result = false
    }
    Future.value(result)
  }
}