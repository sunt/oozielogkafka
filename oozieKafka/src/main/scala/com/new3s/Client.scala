package com.new3s

/**
 * Created by lifeng ai on 2015/12/25 0025.
 */
import com.twitter.finagle.{Http, Service, http}
import com.twitter.util.{Await, Future}

object Client extends App {
  //#builder
  val client: Service[http.Request, http.Response] = Http.newService("localhost:8080,localhost:8081")
  //#builder
  //#dispatch
  val request = http.Request(http.Method.Get, "/ai")
  //request.host = "www.scala-lang.org"
  val response: Future[http.Response] = client(request)
  //#dispatch
  //#callback
  response.onSuccess { resp: http.Response =>
    println("GET success: " + resp.getContentString())
  }
  response.onFailure {
    cause: Throwable => println("f failed with " + cause) //匿名函数
  }

  println("request was sent!")
  println("waiting for result!")
  Await.ready(response)
  println("done!")
  //#callback
}
