package com.new3s

import com.sqlTest.{OozieResultHbase, OozieHbase}
import com.twitter.finagle.http.{Request, Response, Version}
import com.twitter.finagle.{Service, SimpleFilter, http}
import com.twitter.logging.Logger
import com.twitter.util.{Await, Future}


/**
 * Created by lifeng ai on 2015/12/25 0025.
 */
object OozieHbaseServer extends App {
  val log = Logger.apply()

  class HandleExceptions extends SimpleFilter[Request, Response] {

    def apply(request: Request, service: Service[Request, Response]) = {
      log.info("run filter")

      // `handle` asynchronously handles exceptions.
      service(request) handle { case error =>
        val statusCode = error match {
          case _: IllegalArgumentException =>
            http.Status.Forbidden
          case _ =>
            http.Status.InternalServerError
        }
        val errorResponse = http.Response(request.version, statusCode)
        errorResponse.contentString = error.getStackTraceString

        errorResponse
      }
    }
  }


  class SqlQueryService extends Service[Request, Response] {
    def apply(req: Request) = {
      val url = req.uri
      log.info(s"request url---------->>$url")
      //  /?job_task_id&message
      val params = url.split("/")(1).split("&")
      val job_task_id = params(0)
      log.info(s"request job_task_id is---------->>$job_task_id")
      val start_time = params(1)
      log.info(s"request start_time is---------->>$start_time")
      val end_time = params(2)
      log.info(s"request end_time is---------->>$end_time")
      val status = params(3)
      log.info(s"request status is---------->>$status")
      val output = params(4)
      log.info(s"request status is---------->>$output")

      val excuteOp = OozieResultHbase
      val result: Future[Boolean] = excuteOp.writeHbase(job_task_id, start_time, end_time, status, output)
      val responseFuture: Future[Response] = result.flatMap(str => convertStrToHttpResponse(str, req.version))
      responseFuture
    }
  }

  /**
   *
   * @param str
   * @param httpVersion
   * @return
   */
  def convertStrToHttpResponse(str: Boolean, httpVersion: Version): Future[http.Response] = {
    val httpResponse = http.Response(httpVersion, http.Status.Ok)
    httpResponse.setContentString(s"$str")
    Future.value(httpResponse)
  }

  val handleExceptions = new HandleExceptions
  val sqlQueryService = new SqlQueryService

  val service: Service[http.Request, http.Response] = handleExceptions andThen sqlQueryService
  //  val address: SocketAddress = new InetSocketAddress(6379)
  //  val monitor: Monitor = new Monitor {
  //    def handle(t: Throwable): Boolean = {
  //      // do something with the exception
  //      true
  //    }
  //  }
  //  val twitter: Service[Request, Response] = Http.client
  //    .withMonitor(monitor)
  //    .withSessionPool
  //    .maxSize(1)
  //    .newService("192.168.2.16:8081,127.0.0.1:8081")
  //对外提供service为http服务
  val server = com.twitter.finagle.Http.serve(":6379", service) // protocol.server(address,service)

  Await.ready(server) // waits until the server resources are released
}
