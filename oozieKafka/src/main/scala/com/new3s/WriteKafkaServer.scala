package com.new3s

import java.net.{SocketAddress, InetSocketAddress}

import com.sqlTest.util.ConstUtil
import com.sqlTest.{OozieKafka}
import com.twitter.finagle.http.{Version, Request, Response}
import com.twitter.finagle.{Service, SimpleFilter, http}
import com.twitter.logging.Logger
import com.twitter.util.{Await, Future}


/**
 * Created by lifeng ai on 2015/12/25 0025.
 */
object WriteKafkaServer extends App {
  val log = Logger.apply()

  class HandleExceptions extends SimpleFilter[Request, Response] {

    def apply(request: Request, service: Service[Request, Response]) = {
      log.info("run filter")

      // `handle` asynchronously handles exceptions.
      service(request) handle { case error =>
        val statusCode = error match {
          case _: IllegalArgumentException =>
            http.Status.Forbidden
          case _ =>
            http.Status.InternalServerError
        }
        val errorResponse = http.Response(request.version, statusCode)
        errorResponse.contentString = error.getStackTraceString

        errorResponse
      }
    }
  }


  class SqlQueryService extends Service[Request, Response] {
    def apply(req: Request) = {
      val url = req.uri
      log.info(s"request url---------->>$url")
      //  /?job_task_id&message
      val params = url.split("/")(1).replace("?", "").split("&")
      val job_task_id = params(0)
      log.info(s"request jon_task_id is---------->>$job_task_id")
      val file_name = params(1)
      log.info(s"request file_name is---------->>$file_name")
      val log_order = params(2)
      log.info(s"request log_order is---------->>$log_order")
      val log_info = if (new ConstUtil().isEmpty(params(3))) params(3) else "Empty"
      log.info(s"request log_info is---------->>$log_info")


      val excuteOp = OozieKafka
      val result: Future[Boolean] = excuteOp.writeToKafka(job_task_id, file_name, log_order, log_info)
      val responseFuture: Future[Response] = result.flatMap(str => convertStrToHttpResponse(str, req.version))
      responseFuture
    }
  }

  /**
   *
   * @param str
   * @param httpVersion
   * @return
   */
  def convertStrToHttpResponse(str: Boolean, httpVersion: Version): Future[http.Response] = {
    val httpResponse = http.Response(httpVersion, http.Status.Ok)
    httpResponse.setContentString(s"$str")
    Future.value(httpResponse)
  }

  val handleExceptions = new HandleExceptions
  val sqlQueryService = new SqlQueryService

  val service: Service[http.Request, http.Response] = handleExceptions andThen sqlQueryService
  //  val address: SocketAddress = new InetSocketAddress(6379)
  //  val monitor: Monitor = new Monitor {
  //    def handle(t: Throwable): Boolean = {
  //      // do something with the exception
  //      true
  //    }
  //  }
  //  val twitter: Service[Request, Response] = Http.client
  //    .withMonitor(monitor)
  //    .withSessionPool
  //    .maxSize(1)
  //    .newService("192.168.2.16:8081,127.0.0.1:8081")
  //对外提供service为http服务
  val server = com.twitter.finagle.Http.serve(":6377", service) // protocol.server(address,service)

  Await.ready(server) // waits until the server resources are released
}
