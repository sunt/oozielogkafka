import _root_.sbtassembly.Plugin.AssemblyKeys
import AssemblyKeys._
import _root_.sbtassembly.Plugin._

assemblySettings

name := "oozieKafka"

version := "1.0"

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  "com.twitter" %% "finagle-http" % "6.34.0",
  "com.twitter" %% "util-core" % "6.30.0",
  "com.twitter" %% "finagle-mysql" % "6.33.0",
  "org.apache.kafka" % "kafka_2.11" % "0.8.2.0",
  "org.apache.hadoop" % "hadoop-common" % "2.6.0",
  "org.apache.hbase" % "hbase-common" % "0.99.0",
  "org.apache.thrift" % "libthrift" % "0.8.0",
  "com.google.code.gson" % "gson" % "2.3.1",
  "org.scala-lang" % "scala-library" % "2.11.7",
  "org.slf4j" % "slf4j-api" % "1.7.6",
  "org.apache.hbase" % "hbase-server" % "0.99.0"

)

javacOptions ++= Seq("-encoding", "UTF8")

credentials += Credentials(Path.userHome / ".sbt" / "credentials")


publishTo := {
  val nexus = "http://192.168.2.252:8081/nexus/"
  if (isSnapshot.value)
    Some("snapshots" at nexus + "content/repositories/snapshots")
  else
    Some("releases" at nexus + "content/repositories/releases")
}

mergeStrategy in assembly := {
  case PathList("javax", "servlet", xs@_*) => MergeStrategy.first
  case PathList(ps@_*) if ps.last endsWith ".html" => MergeStrategy.first
  case PathList("META-INF", "MANIFEST.MF") => MergeStrategy.discard
  case PathList("META-INF", xs@_*) => MergeStrategy.first
  case PathList(ps@_*) if ps.last endsWith ".txt" => MergeStrategy.first
  case PathList(ps@_*) if ps.last endsWith ".class" => MergeStrategy.first
  case PathList(ps@_*) if ps.last endsWith ".properties" => MergeStrategy.first
  case "application.conf" => MergeStrategy.concat
  case "unwanted.txt" => MergeStrategy.discard
  case x =>
    val oldStrategy = (mergeStrategy in assembly).value
    oldStrategy(x)
}